package com.pros.priceoptimizer.parser

import com.pros.priceoptimizer.dataload.PriceRecord

// A regular expression parser for a simple DSL for price records
// TODO: I'd like to separate attributes using commas
class PriceRecordDsl extends BasicParsers {
  var records : List[PriceRecord] = List()
  
  def expr = rep(priceRecord ~ attributes) ^^ { (x) => records }
  def priceRecord = 
    keyword("price") ~> keyword("record") ~> keyword("for") ~> complexWord ^^ createRecord

  def attributes = rep(attribute)
  def attribute = 
    keyword("and") | valid | component | level | usage | extracted
  def valid = validFrom ~> (validTo?)
  def validFrom = keyword("valid") ~> keyword("from") ~> prosDate ^^ setFrom
  def validTo = keyword("to") ~> prosDate ^^ setTo
  def component = keyword("component") ~> keyword("type") ~> singleWord ^^ setComponentType
  def level = keyword("price") ~> keyword("level") ~> singleWord ^^ setPriceLevel
  def usage = keyword("used") ~> keyword("as") ~> singleWord ^^ setUsage 
  def extracted = keyword("extracted") ~> keyword("on") ~> (prosDate | prosDateTime) ^^ setExtracted
  
  def createRecord(dimensions : String) : PriceRecord = {
    var record = new PriceRecord
    record.dimensionValues = dimensions
    records = record::records
    currentRecord
  }
  def currentRecord = records.last
  
  def setComponentType(component : String) = currentRecord.componentType = component
  def setPriceLevel(level : String) = currentRecord.priceLevel  = level
  def setExtracted(extractionTime : String) = currentRecord.extractionTime = Some(extractionTime)
  def setUsage(usage : String) = currentRecord.usageType = Some(usage)
  def setFrom(date : String) = currentRecord.validRange = (date.toLong, currentRecord.validRange._2)
  def setTo(date : String) = currentRecord.validRange = (currentRecord.validRange._1, Some(date.toLong))
  
  def parse_price(expression : String) = parseAll(expr, expression)
}