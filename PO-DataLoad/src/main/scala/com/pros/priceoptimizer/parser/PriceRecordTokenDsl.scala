package com.pros.priceoptimizer.parser

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import com.pros.priceoptimizer.dataload.PriceRecord

// This is a failed try, the parser works but the string contents can be invalid
object PriceRecordTokenDsl extends StandardTokenParsers {
    var records : List[PriceRecord] = List()
  
	lexical.reserved +=
	  ("price", "record", "for",
	      "sell", "at", "and",
	      "valid", "from", "to", 
	      "component", "level", 
	      "used", "as", "discount", "rebate",
	      "with", "dimensions",
	      "extracted", "on")
	      
	lexical.delimiters += ("(", ")", ",")
	
	lazy val parse_price = price_record ~ attributes
	lazy val price_record = keyword("price") ~> keyword("record") ^^ createPrice
	lazy val attributes = repsep(attribute, "and" | ",")
	lazy val attribute = sell | valid | component | level | usage | dimensions | extracted
	lazy val sell =  "sell" ~> "at" ~> numericLit ^^ setPrice
	lazy val valid = valid_from ~> valid_to?
	lazy val valid_from = "valid" ~> "from" ~> numericLit ^^ validFromDate
	lazy val valid_to = "to" ~> numericLit ^^ validToDate
	lazy val component = "component" ~> "type" ~> ident ^^ echo _
	lazy val level = "price" ~> "level" ~> ident
	lazy val usage = "used" ~> "as" ~> ("price" | "discount" | "rebate")
    lazy val dimensions = "for" ~> (("dimensions")?) ~> ident
	lazy val extracted = "extracted" ~> "on" ~> numericLit ^^ setExtractedOn
	
	def parseAll[T](p : Parser[T], in : String) : ParseResult[T] = phrase(p)(new lexical.Scanner(in))
	
	def echo(msg : String) : Unit = println _
	def createPrice(name : String) = records = new PriceRecord()::records
	def setPrice(value:String) : Unit = records.head.price = value.toInt
	def setExtractedOn(value:String) : Unit = records.head.extractionTime  = Some(value)
	def validFromDate(value:String) : Unit = records.head.validRange = (value.toLong, records.head.validRange._2)
	def validToDate(value:String) : Unit = records.head.validRange = (records.head.validRange._1, Some(value.toLong))
}