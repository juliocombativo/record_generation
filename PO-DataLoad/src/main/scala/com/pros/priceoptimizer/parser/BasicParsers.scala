package com.pros.priceoptimizer.parser

import scala.util.parsing.combinator.RegexParsers
import java.util.regex.Pattern

// Defining some basic regular expressions
class BasicParsers extends RegexParsers {
	def singleWord = """[a-zA-Z_][a-zA-Z_0-9]*""".r
	def anyChar = """\S""".r
	def complexWord = """[\S^,]*""".r
	def prosDate = """\d\d\d\d[0-1]\d[0-3]\d""".r
	def prosDateTime = """\d\d\d\d[0-1]\d[0-3]\d \d\d:\d\d[:\d\d]?""".r
	def floatNumber : Parser[Float] = """-?(\d+(\.\d*)?|\d*\.\d+)([eE][+-]?\d+)?[fFdD]?""".r ^^ toFloat
	
	implicit def keyword(identifier : String) : Parser[Unit] = (identifier + "\\b").r ^^ noop
	
	def toFloat(value : String) = value.toFloat
	def noop(name : String) : Unit = { }
}