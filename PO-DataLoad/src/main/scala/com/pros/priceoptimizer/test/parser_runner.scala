package com.pros.priceoptimizer.test

import com.pros.priceoptimizer.dataload.PriceRecord
import com.pros.priceoptimizer.dataload.DataLoadUtils
import scala.xml.PrettyPrinter
import com.pros.priceoptimizer.parser.PriceRecordDsl
import com.pros.priceoptimizer.parser.PriceRecordDsl
import scala.util.parsing.combinator.RegexParsers
import com.pros.priceoptimizer.parser.PriceRecordDsl
import com.pros.priceoptimizer.parser.PriceRecordDsl

// Simple sample for simple text parsing
object parser_runner extends RegexParsers {
  def element(name: String): XMLElement = new XMLElement(name);

  def main(args: Array[String]): Unit = {
    var dsl : PriceRecordDsl = 
      test("price record for LT1100/1100|1100 component type PR02\n valid from 20130201 price record for MT400")
  }
  
  def test(matchString : String) : PriceRecordDsl = {
    var dsl = new PriceRecordDsl
    dsl.parse_price (matchString) match {
      case dsl.Failure(msg, t) => 
        println("Failure"); println(msg);
      case dsl.Error(msg, _) => 
        println("Error"); println(msg)
      case dsl.Success(x, _) => 
        var xmlElem = DataLoadUtils.recordSet("Cond_Table_Data_Set", x.map(f => f.toXml))
        var printer = new PrettyPrinter(200, 2)
        println(printer.format(xmlElem))
    }
    dsl
  }

}