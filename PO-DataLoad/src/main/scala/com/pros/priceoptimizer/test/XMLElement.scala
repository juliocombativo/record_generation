package com.pros.priceoptimizer.test

// Another failed attempt to generate XML, I forgot that Scala does it for you!
class XMLElement (name : String) {
  var children : Map[String, XMLElement] = Map()
  var attributes : Map[String, String] = Map()
  var content : String = ""

  def getName() : String = name;
  def withAttribute(name : String, value : String) : XMLElement = {
    attributes += ((name, value))
    this
  }

  def withChild(child : XMLElement) : XMLElement = {
    children += ((child.getName(), child))
    this
  }

  def withChildren(newElements : List[XMLElement]) : XMLElement = {
    newElements.foreach (x => withChild(x))
    this
  }
  
  def withContent(newContent : String) : XMLElement = {
    content = newContent
    this
  }

  override def toString() : String = {
    var pieces : List[String] = List("<", name, appendAttributes)
    var isEmpty = children.isEmpty && "".equals(content)

    if (isEmpty) pieces = pieces:::List("/")
    pieces = pieces:::List(">", content, appendChildren)
    if (!isEmpty) pieces = pieces:::List("</", name, ">")

    pieces mkString ""
  }

  def appendAttributes() : String = attributes.toList.foldLeft(new StringBuilder())((builder, f) => builder.append(String.format(" %s=\"%s\"", f._1, f._2))).toString

  def appendChildren() : String = children.values mkString("")
}