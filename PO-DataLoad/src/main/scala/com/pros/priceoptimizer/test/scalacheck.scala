package com.pros.priceoptimizer.test

import scala.xml.PrettyPrinter
import com.pros.priceoptimizer.dataload.PriceRecord
import org.scalacheck.Arbitrary
import org.scalacheck.Gen
import org.scalacheck.Gen.freqTuple
import org.scalacheck.Gen.frequency
import org.scalacheck.Gen.oneOf
import org.scalacheck.Prop.forAll
import org.scalacheck.Prop
import scala.Some
import com.pros.priceoptimizer.generator.custom.Product
import scala.collection.Seq
import com.pros.priceoptimizer.generator.UniqueHelper
import com.pros.priceoptimizer.generator.UniqueHelper

// Some ideas regarding tests using scala check
object scalacheck {
  import org.scalacheck._
  import Prop.{forAll, BooleanOperators, all}
  import Gen._
  import com.pros.priceoptimizer.generator.PriceRecordGenerator._
  import Arbitrary.arbitrary
  
  implicit lazy val arbPriceRecord : Arbitrary[PriceRecord] = Arbitrary(genPrice)
  
  var printer = new PrettyPrinter(200, 2)
  
  def generateValues[T](generator : Gen[T]) : List[T] = {
    generateValues(generator, 100)
  }
  
  def generateValues[T](generator : Gen[T], n : Int) : List[T] = {
    var results : List[T] = List()
    val prop = forAll(generator) { 
      p =>
      	Prop.collect(p) { results = results :+ p; true }
        true 
    }
    
    val params = Test.Parameters.default.withMinSize(n).withMaxSize(n)
    var result : Test.Result = Test.check(prop)(param=>params)
    
    results
  }
  
  /*def unique[T](generator : Gen[T]) : (() => Gen[T]) = {
    var helper = new UniqueHelper[T]
    () => {
      for {
        result <- generator.withFilter(helper.apply)
      } yield {
        result 
      }
    }
  }*/
  
  def unique[T](generator : Gen[T]) : Gen[T] = {
    var helper = new UniqueHelper[T]
    doUnique(generator, helper)
  }
  
  def doUnique[T](generator : Gen[T], helper : UniqueHelper[T]) : Gen[T] = for {
    result <- generator.withFilter(helper.apply)
  } yield { result }
  
  def generatedValue[T](generator : Gen[T]) : T = {
    var value = generator.sample
    while(!value.isDefined) {
      value = generator.sample
    }
    value.get
  }
  
  def main(args: Array[String]) : Unit = {
    // And now we print tons of those generated price records
    var sizeAdd = forAll { (record: PriceRecord) => println(record.toXml); true }
    sizeAdd.check    
    
    generateValues(genPrice)
  }
}