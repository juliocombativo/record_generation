package com.pros.priceoptimizer.test

import com.pros.priceoptimizer.dataload.PriceRecord
import com.pros.priceoptimizer.dataload.DataLoadUtils
import scala.xml.PrettyPrinter

object runner {
  def element(name: String): XMLElement = new XMLElement(name);

  // Trying XML serialization
  def main(args: Array[String]): Unit = {
    import scala.sys.process._
    
    var record = new PriceRecord

    record.componentType = "PR02"
    record.usageType = Some("Price")
    record.price = 12.345678
    
    var record2 = new PriceRecord
    
    var printer = new PrettyPrinter(200, 2)
    println(printer.format(DataLoadUtils.recordSet("Cond_Tbl_Data_Set", List(record.toXml, record2.toXml))))
    
    val contents = Process("cmd /c dir").lineStream
    contents.foreach { x => println (x) }
    val result = Process("cmd /c dir")!;
    println(result)
  }

}