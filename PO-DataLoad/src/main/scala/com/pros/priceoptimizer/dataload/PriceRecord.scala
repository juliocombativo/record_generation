package com.pros.priceoptimizer.dataload

import scala.xml.Elem
import scala.util.parsing.json.JSONObject
import scala.util.parsing.json.JSONObject
import scala.util.parsing.json.JSONObject
import scala.util.parsing.json.JSONArray
import scala.util.parsing.json.JSON
import scala.util.parsing.json.JSONArray

// POJO that can be converted easily to XML
class PriceRecord {
  var componentType = "A";
  var priceLevel = "B";
  var price = 0.0;
  var dimensionValues = "LT1000/1100"
  var validRange : (Long, Option[Long]) = (DataLoadUtils.today, None)
  var usageType : Option[String] = None
  var action : Option[String] = None
  var extractionTime : Option[String] = None
  
  var scaleStructure : Option[List[Tuple2[Double, Double]]] = None
  
  def toXml() = {
    (<Cond_Tbl_Data_Record>
     <Condition_Type>{componentType}</Condition_Type>
     <Condition_Table_ID>{priceLevel}</Condition_Table_ID>
     <Key_Values>{dimensionValues}</Key_Values>
     {if (usageType isDefined) <Usage>{usageType.get}</Usage>}
     <Valid_From_Date>{validRange._1}</Valid_From_Date>
     {if (validRange._2 isDefined) <Valid_To_Date>{validRange._2.get}</Valid_To_Date> 
      else <Valid_To_Date>{DataLoadUtils.ForeverDate}</Valid_To_Date>}
     {if (action isDefined) <Action>{action.get}</Action>}
     <Condition_Value>{DataLoadUtils.currency(price)}</Condition_Value>
     {if (extractionTime isDefined) <Extraction_Time>{extractionTime}</Extraction_Time> 
      else <Extraction_Time>{DataLoadUtils now}</Extraction_Time>}
     { if(scaleStructure isDefined) scaleXml }
   </Cond_Tbl_Data_Record>)
  }
  
  def toJson() : String = {
    json.toString()
  }
  
  def json : JSONObject = {
    var jsonMap:Map[String, Any] = Map(
      "ComponentType" -> componentType,
      "PriceLevel" -> priceLevel,
      "Scope" -> dimensionValues,
      "ValidFrom" -> validRange._1,
      "ValidTo" -> validRange._2.getOrElse(DataLoadUtils.ForeverDate),
      "Usage" -> usageType.getOrElse("Price"),
      "Action" -> action.getOrElse("Update"),
      "ExtractionTime" -> extractionTime.getOrElse(DataLoadUtils.now))
    
    if (scaleStructure isDefined) {
      scaleStructure match {
        case Some(scaleSteps) => val scales = scaleSteps.map(
            x => JSONObject(Map("ScaleQuantity" -> x._1, "ScaleValue" -> x._2)))
          jsonMap += ("Scales" -> JSONArray(scales.toList))
        case _ => 
      }
    }
        
    return JSONObject(jsonMap)
  }
  
  def scaleXml() = {
    scaleStructure match {
      case Some(scaleSteps) => scaleSteps.map(x => 
        (<Scale_Record>
           <Scale_Quantity>{x._1}</Scale_Quantity>
           <Scale_Value>{x._2}</Scale_Value>
         </Scale_Record>))
      case _ => 
    }
  }
}