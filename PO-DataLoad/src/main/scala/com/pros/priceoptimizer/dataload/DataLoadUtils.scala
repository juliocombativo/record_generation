package com.pros.priceoptimizer.dataload

import java.text.SimpleDateFormat
import java.util.Date
import java.util.GregorianCalendar
import java.util.Calendar
import java.text.NumberFormat
import java.text.DecimalFormat
import scala.xml.Elem
import scala.xml.TopScope
import scala.xml.XML
import scala.xml.NodeSeq
import scala.xml.Group

// I need some content and formating for data loading and generation
object DataLoadUtils {
	val ForeverDate : Long = 99991231
	val dateFormat : SimpleDateFormat = new SimpleDateFormat("yyyyMMdd")
	val hourFormat : SimpleDateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss")
	val currencyFormat : NumberFormat = new DecimalFormat("#.####")
	
	def today() : Long = dateFormat.format(Calendar.getInstance().getTime()).toLong
	
	def now() : String = hourFormat.format(Calendar.getInstance().getTime())
	
	def currency(value : Double) = currencyFormat format value
	
	def recordSet (tagName : String, elements : List[Elem]) = 
	  Elem(null, tagName, scala.xml.Null, TopScope, Group(elements))
}