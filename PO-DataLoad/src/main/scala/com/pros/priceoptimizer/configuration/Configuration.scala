package com.pros.priceoptimizer.configuration

import scala.sys.process._

import java.util.Properties
import java.io.FileInputStream
import scalikejdbc.ConnectionPool
import scalikejdbc.GlobalSettings
import scalikejdbc.LoggingSQLAndTimeSettings
import scalikejdbc.DB

object Configuration {
	List("org.h2.Driver", 
	  "net.sourceforge.jtds.jdbc.Driver",
	  "oracle.jdbc.driver.OracleDriver")
	  
	val (url, user, password) = {
	  val prop = new Properties
	  prop.load(new FileInputStream("config.properties"))
	  
	  (
	    prop.getProperty("app.url"),
	    prop.getProperty("app.user"),
	    prop.getProperty("app.password")
	  )
	}
	
	GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(enabled = false)
	ConnectionPool.singleton(url, user, password)
	
	def db = DB
	
	def runTask(params : String*) {
	  val seq = Seq("cmd", "/c", "prosshell", "runTask") ++ params.toSeq
	  val out = seq.!!
	}
}