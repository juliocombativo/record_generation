package com.pros.priceoptimizer.generator.custom

import scala.xml.XML
import scala.util.parsing.json.JSON
import scala.util.parsing.json.JSONArray
import scala.util.parsing.json.JSONArray
import scala.util.parsing.json.JSONObject
import com.pros.priceoptimizer.test.scalacheck._
import scala.xml.Elem
import scala.xml.Node
import scala.xml.PrettyPrinter

object PoductGenerator {
  import org.scalacheck._
  import Prop._
  import Gen._
  import Parameters._
  
  def main(args: Array[String]) {
    var products : List[Node] = generateValues(genProduct, 400).map(f => f.toXml)
    
    val xmlDoc = <Product_Set>{products}</Product_Set>
    val s:PrettyPrinter = new PrettyPrinter(200, 2)
    
    println(s.format(xmlDoc))
  }
  
  def printXml(product : Product) : Elem = {
    product.color = generatedValue(genColor)
    product.skuSize = generatedValue(genSkuSize)
    product.toXml
  }
  
  def genProduct : Gen[Product] = {
    for {
      desc <- unique(oneOf(genWorkstation, genServer, genLaptop))
      skuSize <- genSkuSize
      color <- genColor
    } yield {
      println(desc._4)
      val product = new Product
      product.category    = desc._1 
      product.subCategory = desc._2
      product.productType = desc._3
      product.id          = desc._4
      product.sku         = desc._5
      product.skuSize     = skuSize
      product.color       = color
      product
    }
  }
  
  def genColor = oneOf(
      "Amaranth", "Amber", "Amethyst", "Apricot", "Aquamarine", "Azure",
      "Baby blue", "Beige", "Black", "Blue", "Blue-green", "Blue-violet",
      "Blush", "Bronze", "Brown", "Burgundy", "Byzantium", "Carmine")
      
  def genSkuSize = oneOf("Small", "Medium", "Big", "Huge")
      
  def genWorkstation : Gen[Tuple5[String, String, String, String, String]] =
    genMap(
      Seq(("DT", "T Workstation"), ("DX", "X Workstation"), ("RX", "Dynamic X"), ("RA", "Dynamic A")), 
      Seq("", "-M", "-X", "-XE"), 
      ("Hardware", "Desktop", "WorkStation"))
  
  def genServer : Gen[Tuple5[String, String, String, String, String]] = 
    genMap(
      Seq(("ST", "Server Serie T"), ("SX", "Server Serie X"), ("SRX", "Server Serie 2000 X"), ("SY", "Server Serie 2000 Y")), 
      Seq("", "-X", "-XE", "-C1", "-C2"), 
      ("Hardware", "Desktop", "Server"))
      
  def genLaptop : Gen[Tuple5[String, String, String, String, String]] =
    genMap(
      Seq(("LTA", "Aurora"), ("LTU", "UFO"), ("LTG", "Gamma Ray"), ("LTZ", "Omega")), 
      Seq("", "-LA", "-BR", "-CH", "JN"), 
      ("Hardware", "Mobile", "Laptop"))
      
  def genNetbook : Gen[Tuple5[String, String, String, String, String]] =
    genMap(
      Seq(("NTA", "Aurora"), ("NTU", "UFO"), ("NTG", "Gamma Ray"), ("NTZ", "Omega")), 
      Seq("", "-LA", "-BR"), 
      ("Hardware", "Mobile", "Netbook"))
  
  def genMap (skuIds : Seq[Tuple2[String, String]], idExts : Seq[String], defaults : Tuple3[String, String, String]) : 
    Gen[Tuple5[String, String, String, String, String]] = for {
      number <- Gen.chooseNum(1, 25)
      hundred <- number * 100
      (sku, id) <- oneOf(skuIds)
      idExt <- oneOf(idExts)
  } yield (defaults._1, defaults._2, defaults._3, s"$sku$hundred$idExt", s"$id $hundred$idExt")
  
  def genCloth : Gen[List[String]] = for {
    color <- genColor
  } yield { List() }
}

class Product {
  var id : String = ""
  var sku : String = ""
  var category : String = ""
  var subCategory : String = ""
  var productType : String = ""
  var color : String = ""
  var skuSize : String = ""
    
  def toXml() = {
  (<Product_Record>
     <Product_ID>{id}</Product_ID>
     <SKU>{sku}</SKU>
     <Product_Type>{productType}</Product_Type>
     <Sub_Category>{subCategory}</Sub_Category>
     <Category>{category}</Category>
     <Color>{color}</Color>
     <Sku_Size>{skuSize}</Sku_Size>
   </Product_Record>)
  }
}