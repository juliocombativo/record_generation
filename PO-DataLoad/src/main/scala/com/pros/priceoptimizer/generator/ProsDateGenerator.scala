package com.pros.priceoptimizer.generator

object ProsDateGenerator {
  import org.scalacheck._
  import Gen._
  
  def adjustedDay(month: Long, day : Long) = (day, month) match {
    case february if (month == 2 && day > 28) => 28
    case _30days if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11)) => 30 
    case _ => day
  }
  
  def adjustAdapter(year : Long, month : Long, day :Long) : Long = 
    (year * 10000) + (month * 100) + adjustedDay(month, day)
  
  def genProsDate : Gen[Long] = for {
    year <- Gen.chooseNum(2014, 2055)
    month <- Gen.chooseNum(1, 12)
    day <- Gen.chooseNum(1, 31)
  } yield adjustAdapter(year, month, day)
}