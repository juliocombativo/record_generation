package com.pros.priceoptimizer.generator

class UniqueHelper[T] {
  private[this] var history : Set[T] = Set()
  
  def apply(t : T) : Boolean = {
    history.contains(t) match {
      case true => 
        false
      case false =>
        history = history + t
        true
    }
  }
}