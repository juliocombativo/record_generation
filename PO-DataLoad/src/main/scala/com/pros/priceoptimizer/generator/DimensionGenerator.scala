package com.pros.priceoptimizer.generator

//TODO: Implement some DB access rules
object DimensionGenerator {
  import com.pros.priceoptimizer.configuration.Configuration._
  import org.scalacheck._
  import scalikejdbc._
  import Gen._
  
  // TODO: Haven't checked SQL Server and Oracle queries
  def selectTop(table : String, column: String, max : Int) : String = {
    url match {
      case h2 if url.contains(":h2:") => 
        s"SELECT DISTINCT $column FROM $table ORDER BY 1 LIMIT $max"
      case oracle if url.contains(":jtds:") => 
        s"SELECT TOP $max DISTINCT $column from $table ORDER BY 1"
      case sqlServer if url.contains(":oracle:") => 
        s"SELECT DISTINCT $column FROM $table WHERE ROWNUM <= $max ORDER BY 1"
    }
  }
  
  def genFromList(values : String*) = oneOf(values)
  
  def genFromDbTable(table : String, column: String) = {
    val values = db readOnly { implicit session =>
      val sqlString = selectTop(table, column, 100)
      SQL(sqlString).map(rs => rs.string(column)).list.apply
    }
    genFromList(values.toSeq:_*)
  }
}