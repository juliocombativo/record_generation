package com.pros.priceoptimizer.generator

import com.pros.priceoptimizer.dataload.PriceRecord
import org.scalacheck.Gen
import org.scalacheck.Gen.const
import org.scalacheck.Gen.freqTuple
import org.scalacheck.Gen.frequency
import org.scalacheck.Gen.oneOf

object PriceRecordGenerator {
  import com.pros.priceoptimizer.generator.ProsDateGenerator._
  import com.pros.priceoptimizer.generator.DimensionGenerator._
  import org.scalacheck._
  import Gen._
  
  def genAction = oneOf("Insert", "Delete", "Replace")
  
  def genScale : Gen[Option[List[Tuple2[Double, Double]]]] = for {
    n <- Gen.chooseNum(0, 5)
    thresholds <- Gen.listOfN(n, Gen.chooseNum(1, 100)) suchThat(l => l.distinct.size == l.size)
    offsets <- Gen.listOfN(n, Gen.chooseNum(100.0, 1000.0)) suchThat(l => l.distinct.size == l.size)
  } yield { 
    Some(thresholds zip offsets map { 
      case (x, y) => (x.toDouble, y)
    })
  }
  
  def genProductSku = genFromList("LT1000", "LT1100", "WS1001", "ASDF")
  def genVersionId = frequency((1, "1100"), (10, "1000"))
  
  // Look! Generated price records!
  def genPrice : Gen[PriceRecord] = for {
    record <- new PriceRecord
    action <- genAction
    componentType <- oneOf("PR01", "PR02", "PR03", "PR04", "PR05")
    priceLevel <- oneOf("B100", "B101", "B102", "B103", "B104")
    usageType <- oneOf(Some("Price"), Some("Discount"), Some("Rebate"), None)
    startDate <- genProsDate
    endDate <- genProsDate
    useEndDate <- Gen.chooseNum(0, 11)
    price <- Gen.chooseNum(10.0, 1100.0)
    product <- genProductSku
    version <- genVersionId
    scale <- genScale suchThat(_.getOrElse(List()).size > 0)
  } yield {
    record.action = Some(action)
    record.componentType = componentType
    record.validRange = (startDate, if (useEndDate == 11) None else Some(endDate))
    record.priceLevel = priceLevel
    record.price = price
    record.usageType = usageType
    record.dimensionValues = product + "|" + version
    record.scaleStructure = scale
    record
  }
}