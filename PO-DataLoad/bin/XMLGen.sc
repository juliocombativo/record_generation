import scala.collection.immutable.StringOps
import com.pros.priceoptimizer.test.XMLElement

object XMLFactory {
  def element (name : String) : XMLElement = new XMLElement(name);
                                                  //> element: (name: String)com.pros.priceoptimizer.test.XMLElement
  def reduceme (mapa : Map[String, String]) = mapa.toList.foldLeft(new StringBuilder())((builder, f) => builder.append(String.format(" %s=\"%s\"", f._1, f._2)) )
                                                  //> reduceme: (mapa: Map[String,String])StringBuilder
  var m : Map[String, String] = Map(("A" -> "B"), ("C" -> "D"))
                                                  //> m  : Map[String,String] = Map(A -> B, C -> D)
  println(reduceme(m))                            //>  A="B" C="D"
  println(element("El_Record"))                   //> <El_Record/>
  println(element("Los_Records").withAttribute ("A", "B").withAttribute("C", "D").withChild(element("El_Record").withContent("RecordValue")).withChild(element("El_Hijo_Del_Record")))
                                                  //> <Los_Records A="B" C="D"><El_Record>RecordValue</El_Record><El_Hijo_Del_Reco
                                                  //| rd/></Los_Records>
}