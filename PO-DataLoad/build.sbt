name := "PO-DataLoad"

version := "1.0"

scalaVersion := "2.11.2"

resolvers += "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.2"

libraryDependencies += "org.scala-lang" % "scala-parser-combinators" % "2.11.0-M4"

libraryDependencies += "org.scalacheck" % "scalacheck_2.11" % "1.11.6"

libraryDependencies ++= Seq(
  "org.scalikejdbc" %% "scalikejdbc"       % "2.1.2",
  "com.h2database"  %  "h2"                % "1.4.181",
  "ch.qos.logback"  %  "logback-classic"   % "1.1.2"
)
